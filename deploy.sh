#!/bin/bash

set -euo pipefail
IFS=$'\n\t'

SELFDIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

(cd $SELFDIR/containers; ./deploy.sh) && (cd $SELFDIR/helm; ./deploy.sh)
