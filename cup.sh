#!/bin/bash

##  do docker-compose up after setting some env vars

REPOROOT="$( git rev-parse --show-toplevel )"
# shellcheck source=./containers/nodejs/vars.sh
source $REPOROOT/containers/nodejs/vars.sh
# shellcheck source=./containers/redis/vars.sh
source $REPOROOT/containers/redis/vars.sh

ACTION="$1"

export ORG
export PROJECT
export APP_NAME
export GIT_BRANCH
export GIT_REF
export GIT_IS_CLEAN
export CHART_VERSION

export NODEJS_PORT
export REDIS_PORT
export LOCAL_NODEJS_CONTAINER_IMAGE
export LOCAL_REDIS_CONTAINER_IMAGE

docker-compose "$ACTION"
