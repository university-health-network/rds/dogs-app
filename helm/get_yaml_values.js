/**
 *	@description: export yaml values as bash environment variables
 *  @note: this file is meant to be invoked by ./get_yaml_values.sh
 **/

var readYaml = require('read-yaml');

const yal = (tuples, fyle, extractor) => {
	return new Promise((resolve,reject) => {
		readYaml(fyle, (err, data) => {
			if (err) {
				reject(err);
			} else {
				tuples.push(extractor(data));
				resolve(tuples);
			}
		});
	});
};

const xal = (tuples, fyle, extractor) => {
	return yal.bind(null, tuples, fyle, extractor);
};


const chartName = (yml) => {
	const tup = [ "CHART_NAME", yml.name ];
	return tup;
};

const print = (tuples) => {;
	tuples.forEach(row => {
		let bashRow = `${row[0]}="${row[1]}"`;
		console.log(bashRow);
	});
	return Promise.resolve(true);
};

const yuk = err => { console.error(err); return false; };

yal([], 'Chart.yaml', chartName)
	.then(tuples => {
		return yal(tuples,'Chart.yaml', data => {
			return [ "CHART_VERSION", data.version ];
		});
	})
	.then(tuples => {
		return yal(tuples, 'values.yaml', data => ["PROJECT", data.project]);
	})
	.then(tuples => {
		return yal(tuples, 'values.yaml', data => ["ORG", data.org]);
	})
	.then(tuples => yal(tuples, 'values.yaml', data => ["APP_NAME", data.app]))
	.then(tuples => yal(tuples, 'values.yaml', data => ["DOMAIN", data.domain]))
	.then(tuples => yal(tuples, 'values.yaml', data => ["NODEJS_PORT", data.nodejs.containerPort]))
	.then(tuples => yal(tuples, 'values.yaml', data => ["NODEJS_CONTAINER_IMAGE_LOCATION", data.nodejs.repository]))
	.then(tuples => yal(tuples, 'values.yaml', data => ["REDIS_PORT", data.redis.containerPort]))
	.then(tuples => yal(tuples, 'values.yaml', data => ["REDIS_CONTAINER_IMAGE_LOCATION", data.redis.repository]))
	.then(print)
	.catch(yuk);
