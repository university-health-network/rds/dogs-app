# shellcheck disable=SC2034

set -euo pipefail
IFS=$'\n\t'

##  this file is meant to be sourced, as a way to set environment variables

HELMDIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
GIT_REF="$(git rev-parse @)"
GIT_BRANCH="$(git symbolic-ref --short HEAD)"
GIT_BRANCH_SAFE="$(echo "$GIT_BRANCH" | tr -dc '[:alnum:]')"
GIT_IS_CLEAN="$(git diff-index --quiet HEAD && echo yes || echo no)"
eval "$(cd $HELMDIR || exit 7; ./get_yaml_values.sh)"
