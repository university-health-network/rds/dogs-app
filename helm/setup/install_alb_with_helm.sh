#!/bin/bash

helm install incubator/aws-alb-ingress-controller \
	--set autoDiscoverAwsRegion=true \
	--set autoDiscoverAwsVpcID=true \
	--set clusterName=rds4 \
	--generate-name
