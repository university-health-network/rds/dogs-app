#!/bin/bash

SELFDIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
# shellcheck source=./vars.sh
source $SELFDIR/vars.sh

docker run -P --rm --name tmp-${APP_NAME}-${CONTAINER_NAME} ${LOCAL_NGINX_CONTAINER_IMAGE}:latest

docker ps
