"use strict";

//	init
let initRan = false;
const init = (ev) => {
	//	output debug info
	const el = document.getElementById('debug');
	const interestingObj = {
		"browser": navigator.appCodeName,
		"build": navigator.buildID
	};
	el.innerHTML = JSON.stringify(interestingObj,null,"\t")
	initRan = true;
};

//	run init when DOM is ready
document.addEventListener('DOMContentLoaded',init);
if (!initRan && ['interactive','complete'].includes(document.readyState)) {
	init();
}
