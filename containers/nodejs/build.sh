#!/bin/bash

SELFDIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
source $SELFDIR/vars.sh

docker build --no-cache=true \
  -t ${NODEJS_CONTAINER_IMAGE_LOCATION}:latest \
  -t ${NODEJS_CONTAINER_IMAGE_LOCATION}:${GIT_BRANCH} \
  -t ${NODEJS_CONTAINER_IMAGE_LOCATION}:${GIT_REF} \
  -t ${LOCAL_NODEJS_CONTAINER_IMAGE}:${GIT_REF} \
  -t ${LOCAL_NODEJS_CONTAINER_IMAGE}:latest \
  --label net.technainstitute.hak/project=${PROJECT} \
  --label net.technainstitute.hak/appName=${APP_NAME} \
  --label net.technainstitute.hak/containerName=${CONTAINER_NAME} \
  --label net.technainstitute.hak/version=${CHART_VERSION} \
  --label net.technainstitute.hak/gitStateIsClean="${GIT_IS_CLEAN}" \
  $SELFDIR
