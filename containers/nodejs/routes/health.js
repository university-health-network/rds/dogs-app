const mime = require('mime-types');
const fs = require('fs');
const counter = require('../helpers/counter.js');
const serve = require('../helpers/serve.js');

const canReadAFile = (fyleName) => {
	return new Promise((resolve, reject) => {
		fs.readFile(__dirname + fyleName, (err, payload) => {
			if (err) {
				let msg = 'Could not read static asset';
				reject({
					err,
					msg
				});
			} else {
				resolve(true);
			}
		});
	});
};

const serveIt = function (req, res) {
	const barf = (err) => {
		res.statusCode = 500;
		res.statusMessage = err.msg;
		return serve(req, res, err.msg, {});
	};
	const good = (countervalue) => {
		res.statusCode = 200;
		res.setHeader('Content-Type', 'text/plain');
		return serve(req, res, countervalue.toString(10), {});
	};
	canReadAFile('/../assets/favicon.ico')
		.then(counter.healthCheck)
		.then(good)
		.catch(barf);
};

module.exports = serveIt;