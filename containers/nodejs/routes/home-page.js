const counter = require('../helpers/counter.js');
const serve = require('../helpers/serve.js');
const render = require('../helpers/render.js');
const settings = require('../helpers/settings.js');
const os = require('os');

const homePage = (req,res) => {

  counter.count().then(n => {
    //  redis counter
    settings.add({n});
    settings.add({
      "host": {
        "name": os.hostname(),
        "arch": os.arch(),
        "release": os.release()
      }
    });
    return render( __dirname + '/../assets/index.html',settings.unwind);
  }).then(html => {
    const options = {
      'type': 'html'
    };
    return serve(req,res,html,options);
  }).catch(err => {
    console.error(err);
    res.statusCode = 500;
    res.setHeader('Content-Type','text/plain');
    res.write('FUCK! ' + req.url + ' was not found and I totally barfed');
    res.end('\n');
  });

};

module.exports = homePage;
