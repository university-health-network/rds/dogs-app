const mime = require('mime-types');
const fs = require('fs');

const serveIt = function(req,res) {

  switch (req.url) {

    case '/funk/weird':
    //  special URLs that need to be handled specially
    break;

    default:
    //  try to read the file from the ./assets folder, or return a 404
    let contentType = mime.lookup(req.url);
    let charset = mime.charset(contentType);
    const options = {};
    if (charset === 'UTF-8') {
      options.encoding = 'utf8';
    } else {
      options.encoding = null;
    }
    fs.readFile( __dirname + '/../assets' + req.url,options,(err,payload) => {
      if (err) {
        res.statusCode = 404;
        res.statusMessage = 'Could not read the file';
        res.setHeader('Content-Type','text/plain');
        res.end('Sowwy! EVerything is bad');
      } else {
        res.statusCode = 200;
        res.setHeader('Content-Type',contentType);
        res.end(payload);
      }
    });
  }
};

module.exports = serveIt;
