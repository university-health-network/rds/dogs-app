"use strict";

const settings = require('./helpers/settings.js');
const counter = require('./helpers/counter.js');
const render = require('./helpers/render.js');
const serve = require('./helpers/serve.js');
const http = require('http');

const routes = {
	"homePage": require('./routes/home-page.js'),
	"catchAll": require('./routes/catch-all.js'),
	"healthCheck": require('./routes/health.js')
};

const server = http.createServer((req, res) => {

	switch (req.url) {
		case '/':
		case '/index.html':
			routes.homePage(req, res);
			break;
		case '/health':
			routes.healthCheck(req, res);
			break;
		default:
			routes.catchAll(req, res);
			break;
	}

});

settings.unwind().then(config => {

	console.log(config);

	server.listen(config.http.port, config.http.host, () => {
		console.log(`Server running at http://${config.http.host}:${config.http.port}/`);
	});

}).catch(console.error);