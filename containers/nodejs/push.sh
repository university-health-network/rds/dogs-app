#!/bin/bash

SELFDIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
# shellcheck source=./vars.sh
source $SELFDIR/vars.sh

docker push ${NODEJS_CONTAINER_IMAGE_LOCATION}:latest
docker push ${NODEJS_CONTAINER_IMAGE_LOCATION}:${GIT_BRANCH}
docker push ${NODEJS_CONTAINER_IMAGE_LOCATION}:${GIT_REF}
