const fs = require('fs');

//  read a template file, and inject values from a promise that resolves to an object of data

module.exports = (template,$scopePromise) => {
  return new Promise(function(good,bad) {
    fs.readFile(template,'utf8',(err,originalContent) => {
      if (err) {
        bad(err);
      } else {
        $scopePromise().then($scope => {
          let returnContent = originalContent;
          returnContent = returnContent.replace('{{ n }}',$scope.n);
          returnContent = returnContent.replace('{{ debug }}',JSON.stringify($scope,null,'\t'));
          good(returnContent);          
        }).catch(bad);
      }
    });
  });
};
