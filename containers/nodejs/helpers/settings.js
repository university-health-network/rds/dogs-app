const git = require('./git.js');

//	only perform expensive unwind() computations once
module.hasBeenResolved = false;

var REDIS_HOST = process.env['REDIS_HOST'] || 'localhost';
var REDIS_PORT = process.env['REDIS_PORT'] || 6379;

const hash = {
	"http": {
		"host": "0.0.0.0",
		"port": 80
	},
	"redis": {
		"host": REDIS_HOST,
		"port": REDIS_PORT
	}
};

const add = (obj) => {
	//	synchronously add data to the hash
	return Object.assign(hash, obj);
};

const getHash = () => {
	//	just return what we have so far. no extra work.
	return hash;
};

const unwind = () => {
	//	a promise that unwinds settings and resolves to an object (the hash)
	if (module.hasBeenResolved) return Promise.resolve(hash);
	return git().then(gitInfo => {
		return new Promise(function (good, bad) {
			hash.git = gitInfo;
			module.hasBeenResolved = true;
			good(hash);
		});
	});
};

module.exports = {
	add,
	unwind,
	getHash
};
