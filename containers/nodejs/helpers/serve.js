module.exports = (req,res,payload,options) => {
  return new Promise((good,bad) => {
    switch(options.type) {
      case 'html':
      res.setHeader('Content-Type','text/html');
      break;
    }
    res.write(payload);
    res.end('\n');
  });
};
