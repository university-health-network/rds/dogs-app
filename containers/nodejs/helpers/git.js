

//	return the info we need from the git repo in our current repo
//	note: only doing read for now. but if is write is required, this is the place


module.exports = () => {
	let r = {
		sha: process.env['HAK_GIT_SHA'],
		branch: process.env['HAK_GIT_BRANCH'],
		state: {
			dirty: process.env['HAK_GIT_STATE_DIRTY']
		}
	};
	return Promise.resolve(r);
};

/*
const gitInfo = require('git-repo-info');
const gitState = require('git-state');
const fspath = require('path');

//	assume we are in a folder two levels above the root (./services/nodejs)
const REPO_ROOT = fspath.dirname(require.main.filename )+'/../../';

module.exports = (path = REPO_ROOT) => {
	//	resolve to a bunch of useful info about our current git repo
	return new Promise(function(good,bad) {
		let r = gitInfo();
		gitState.isGit(path, function (exists) {
			if (!exists) {
				bad(Error('not a git repo'));
			} else {
				gitState.check(path, function (err, result) {
					if (err) {
						bad(err);	
					} else {
						r.state = result;
						good(r);
					}
				});
			}
		});

	});
};
*/