const redis = require('redis');
const settings = require('./settings.js');
const conf = settings.getHash();

const counter = (hitKey) => {
	return new Promise(function (good, bad) {
		const client = redis.createClient(`redis://${conf.redis.host}:${conf.redis.port}`);
		client.on('error', bad);
		client.incr(hitKey, function (err, newcountvalue) {
			if (err) {
				let msg = 'Could not connect to database';
				bad({
					err,
					msg
				});
			} else {
				good(newcountvalue);
			}
		});
	});
};

//  redis-backed page counter. To read the value is to increment it
module.exports = {
	count: counter.bind(this, 'hits/counter'),
	healthCheck: counter.bind(this, 'hits/health')
};