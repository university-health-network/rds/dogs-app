# shellcheck disable=SC2034

set -euo pipefail
IFS=$'\n\t'

SELFDIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
source $SELFDIR/../vars.sh

CONTAINER_NAME=redis
LOCAL_REDIS_CONTAINER_IMAGE="localhost/${ORG}-${PROJECT}-${APP_NAME}-${CONTAINER_NAME}"
