#!/bin/bash

SELFDIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
# shellcheck source=./vars.sh
source $SELFDIR/vars.sh

docker push ${REDIS_CONTAINER_IMAGE_LOCATION}:latest
docker push ${REDIS_CONTAINER_IMAGE_LOCATION}:${GIT_BRANCH}
docker push ${REDIS_CONTAINER_IMAGE_LOCATION}:${GIT_REF}
