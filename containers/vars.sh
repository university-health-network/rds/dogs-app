# shellcheck disable=SC2034

set -euo pipefail
IFS=$'\n\t'

REPOROOT="$(git rev-parse --show-toplevel)"
HELMDIR="$REPOROOT/helm"
# shellcheck source=./helm/vars.sh
source "$HELMDIR/vars.sh"
