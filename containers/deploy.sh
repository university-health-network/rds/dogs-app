#!/bin/bash

#	@description: This script builds and pushes the docker containers. It does not handle deploy to k8s

CONTAINERS_DIR="$(git rev-parse --show-toplevel)/containers"

SUBDIRS="$(find $CONTAINERS_DIR -mindepth 1 -maxdepth 1 -type d -printf "%P\n")"
N_DIRS="$(echo "$SUBDIRS" | wc -l)"

echo "deploying $N_DIRS containers..."

for dir in $SUBDIRS; do
	echo "building and pushing $dir"
	cd "${CONTAINERS_DIR}/${dir}" && ./build.sh && ./push.sh
done
