#!/bin/bash

DIR=$(dirname $0)

(cd $DIR/nodejs/; ./build.sh && ./push.sh)
(cd $DIR/redis/; ./build.sh && ./push.sh)
