#!/bin/bash

## @description: list all the places where version is specified.

GITROOT=$(git rev-parse --show-toplevel)
cd "$GITROOT" || exit 9

declare -a ITEMS=(
  'git tags'
  'grep -i version helm/Chart.yaml'
  'grep -i version helm/package.json'
  'grep -i version containers/nodejs/package.json'
);

for line in "${ITEMS[@]}"; do
  cd "$GITROOT" || exit 9
  printf "%s\t%s\n" "$line" "$(eval "$line")"
done
